package com.kshrd.srsecurityspringdemo.security;

import com.kshrd.srsecurityspringdemo.model.AuthUser;
import com.kshrd.srsecurityspringdemo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserDetailServiceImp implements UserDetailsService {

    @Autowired
    StudentRepository studentRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

//        User
        AuthUser loginUser = studentRepository.findUserByUsername(username);


        if (loginUser==null)
            throw new UsernameNotFoundException("Sorry! This user doesn't exist in this universe");

        // roles
   Collection<? extends GrantedAuthority> authorities = loginUser.getRoles().stream()
                .map(e->new SimpleGrantedAuthority(e))
                .collect(Collectors.toList());


   ///
        System.out.println("Here is the authority of the user : ");
        authorities.stream()
                .forEach(System.out::println);

        return new UserDetailImp(loginUser.getId(), loginUser.getUsername(),loginUser.getPassword(),authorities);
    }
}
