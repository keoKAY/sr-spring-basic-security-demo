package com.kshrd.srsecurityspringdemo.configuration;

import com.kshrd.srsecurityspringdemo.security.UserDetailServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {


    @Autowired
    UserDetailServiceImp userDetailService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

    /*    auth.inMemoryAuthentication().withUser("handsome user").password(passwordEncoder().encode("12345")).roles("HANDSOME");
        auth.inMemoryAuthentication().withUser("admin user").password("{noop}12345").roles("ADMIN");
        auth.inMemoryAuthentication().withUser("normal user").password("{noop}12345").roles("USER");
*/
        //        super.configure(auth);

        auth.userDetailsService(userDetailService)
                .passwordEncoder(passwordEncoder());

    }

    @Bean
    public PasswordEncoder passwordEncoder(){
      // return NoOpPasswordEncoder.getInstance();
       return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        super.configure(http);

        http.authorizeRequests()
                .antMatchers("/public").permitAll()
//                .antMatchers("/foradmin").hasRole("ADMIN")
//                .antMatchers("/foruser").hasRole("USER")
//                .antMatchers("/forhandsome").hasRole("HANDSOME")
                .anyRequest()
                .fullyAuthenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .and()
                .logout()
                .logoutSuccessUrl("/");

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }
}
